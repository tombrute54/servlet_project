package com.example.servletProject.model.dao;

import com.example.servletProject.model.entity.Feedback;

import java.util.List;

public interface FeedbackDAO extends GenericDAO<Feedback> {

    void addFeedback(Feedback feedback) throws DaoException;

    Feedback findById(int id) throws DaoException;

    List<Feedback> findByServiceId(int serviceId) throws DaoException;

    void updateFeedback(int feedbackId, Feedback feedback) throws DaoException;

    Feedback removeFeedbackById(int id) throws DaoException;

}

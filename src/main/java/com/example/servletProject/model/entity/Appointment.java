package com.example.servletProject.model.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Appointment {


    public enum Status {

        NEED("need"),

        CONFIRMED("confirmed"),

        CANCELLED("cancelled"),

        DONE("done");

        private final String statusName;

        Status(String statusName) {
            this.statusName = statusName;
        }


        public String getStatusName() {
            return statusName;
        }
    }


    private int id;
    private LocalDateTime actionDateTime;
    private BigDecimal price;
   // private Status status;
    private int estimate;
    private List<Account> accounts = new ArrayList<>();
    private List<Service> serviceList = new ArrayList<>();
    private List<Status> statusList = new ArrayList<>();


    public List<Status> getStatusList() {
        return statusList;
    }


    public int getId() {
        return id;
    }


    public LocalDateTime getActionDateTime() {
        return actionDateTime;
    }


    public BigDecimal getPrice() {
        return price;
    }


//    public Status getStatus() {
//        return status;
//    }


    public List<Account> getAccounts() {
        return accounts;
    }


    public List<Service> getServices() {
        return serviceList;
    }

    public int getEstimate() {
        return estimate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Appointment)) {
            return false;
        }

        Appointment that = (Appointment) o;

        if (getId() != that.getId()) {
            return false;
        }
        if (getActionDateTime() != null ? !getActionDateTime().equals(that.getActionDateTime()) : that.getActionDateTime() != null) {
            return false;
        }
        if (getPrice() != null ? !getPrice().equals(that.getPrice()) : that.getPrice() != null) {
            return false;
        }
//        if (getStatus() != that.getStatus()) {
//            return false;
//        }
        if (getAccounts() != null ? !getAccounts().equals(that.getAccounts()) : that.getAccounts() != null) {
            return false;
        }
        return Objects.equals(serviceList, that.serviceList);
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getActionDateTime() != null ? getActionDateTime().hashCode() : 0);
        result = 31 * result + (getPrice() != null ? getPrice().hashCode() : 0);
//        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + (getAccounts() != null ? getAccounts().hashCode() : 0);
        result = 31 * result + (serviceList != null ? serviceList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", actionDateTime=" + actionDateTime +
                ", price=" + price +
//                ", status=" + status +
                ", estimate=" + estimate +
                ", Accounts=" + accounts +
                ", serviceList=" + serviceList +
                '}';
    }


    public static AppointmentBuilder newBuilder() {
        return new Appointment().new AppointmentBuilder();
    }


    public class AppointmentBuilder {

        private AppointmentBuilder() {
        }


        public AppointmentBuilder setId(int id) {
            Appointment.this.id = id;
            return this;
        }


        public AppointmentBuilder setAccounts(List<Account> Accounts) {
            Appointment.this.accounts = Accounts;
            return this;
        }


        public AppointmentBuilder setActionDateTime(LocalDateTime actionDateTime) {
            Appointment.this.actionDateTime = actionDateTime;
            return this;
        }


        public AppointmentBuilder setPrice(BigDecimal price) {
            Appointment.this.price = price;
            return this;
        }


        public AppointmentBuilder setEstimate(int estimate) {
            if (estimate >= 0 && estimate <= 10) {
                Appointment.this.estimate = estimate;
            }
            return this;
        }



//        public AppointmentBuilder setStatus(Appointment.Status status) {
//            Appointment.this.status = status;
//            return this;
//        }


        public AppointmentBuilder setServices(List<Service> serviceList) {
            Appointment.this.serviceList = serviceList;
            return this;
        }


        public Appointment build() {
            return Appointment.this;
        }
    }

}


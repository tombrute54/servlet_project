package com.example.servletProject.model.entity;

import java.time.LocalDateTime;


public class Feedback {
    private int id;
    private Service service;
    private LocalDateTime commentDate;
    private String comment;



    public int getId() {
        return id;
    }


    public Service getService() {
        return service;
    }


    public LocalDateTime getCommentDate() {
        return commentDate;
    }


    public String getComment() {
        return comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Feedback)) {
            return false;
        }

        Feedback feedback = (Feedback) o;

        if (getId() != feedback.getId()) {
            return false;
        }
        if (getService() != null ? !getService().equals(feedback.getService()) : feedback.getService() != null) {
            return false;
        }
        if (getCommentDate() != null ? !getCommentDate().equals(feedback.getCommentDate()) : feedback.getCommentDate() != null) {
            return false;
        }
        return getComment() != null ? getComment().equals(feedback.getComment()) : feedback.getComment() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getService() != null ? getService().hashCode() : 0);
        result = 31 * result + (getCommentDate() != null ? getCommentDate().hashCode() : 0);
        result = 31 * result + (getComment() != null ? getComment().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", service=" + service +
                ", commentDate=" + commentDate +
                ", comment='" + comment + '\'' +
                '}';
    }


    public static FeedbackBuilder newBuilder() {
        return new Feedback().new FeedbackBuilder();
    }


    public class FeedbackBuilder {

        private FeedbackBuilder() {
        }


        public FeedbackBuilder setId(int id) {
            Feedback.this.id = id;
            return this;
        }



        public FeedbackBuilder setService(Service service) {
            Feedback.this.service = service;
            return this;
        }


        public FeedbackBuilder setCommentDate(LocalDateTime commentDate) {
            Feedback.this.commentDate = commentDate;
            return this;
        }


        public FeedbackBuilder setComment(String comment) {
            Feedback.this.comment = comment;
            return this;
        }


        public Feedback build() {
            return Feedback.this;
        }
    }
}
package com.example.servletProject.model.dao;

import com.example.servletProject.model.entity.Role;

public interface RoleDAO extends GenericDAO<Role> {

    Role findById(int id) throws DaoException;

    Role findByName(String name) throws DaoException;

    Role createRole (Role role) throws DaoException;

    Role updateRole (int id, String name) throws DaoException;

    Role removeRole (int id) throws  DaoException;
}

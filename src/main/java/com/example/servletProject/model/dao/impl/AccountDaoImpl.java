package com.example.servletProject.model.dao.impl;

import com.example.servletProject.model.dao.AccountDAO;
import com.example.servletProject.model.dao.DaoException;
import com.example.servletProject.model.dao.mapper.AccountMapper;
import com.example.servletProject.model.entity.Account;
import com.example.servletProject.model.entity.Appointment;

//import java.sql.SQLException;
//import java.util.List;

import java.sql.*;
import java.util.*;

public class AccountDaoImpl implements AutoCloseable, AccountDAO {

    private static final String CREATE_USER = " INSERT INTO user" +
            " (login_email, password_hash, first_name, last_name, " +
            " phone_number, user_role, rating) VALUES " +
            " (?, ?, ?, ?, ?, ?, ?); ";

    private static final String FIND_BY_ID = " SELECT * FROM user" +
            " where id = ? ";

    private static final String FIND_BY_LOGIN = " SELECT * FROM user" +
            " where login_email = ? ";

    private static final String FIND_BY_NAME = " SELECT * FROM user WHERE first_name = ? ";

    private static final String FIND_BY_RATING = " SELECT * FROM user WHERE rating = ? ";

    private static final String FIND_ALL = " SELECT * FROM user ";

    private static final String UPDATE_USER = "UPDATE user SET login_email = ?, " +
            " password_hash = ?, first_name = ?, last_name = ?, phone_number = ?, " +
            " user_role = ?, rating = ? WHERE id = ? ";

    private static final String REMOVE = "DELETE FROM user WHERE id = ?" ;

    private final Connection connection;

    public AccountDaoImpl(Connection connection) {
        this.connection = connection;
    }


    @Override
    public Account registerAccount(Account account) throws DaoException {
        try (PreparedStatement preparedStatement = connection.
                prepareStatement(CREATE_USER)) {
            setAccountParameters(account, preparedStatement);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return account;
    }

    @Override
    public Account findById(int id) throws DaoException {
        Optional<Account> optionalAccount = Optional.empty();
        try (PreparedStatement preparedStatement = connection.
                prepareCall(FIND_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            AccountMapper userMapper = new AccountMapper();
            if (resultSet.next()) {
                optionalAccount = Optional.ofNullable(userMapper.extractFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return optionalAccount.orElse(Account.newAccountBuilder().build());
    }

    @Override
    public Account findByLogin(String login) throws DaoException {
        Optional<Account> optionalAccount1 = Optional.empty();
        try (PreparedStatement preparedStatement = connection.
                prepareCall(FIND_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet;
            resultSet = preparedStatement.executeQuery();
            AccountMapper accountMapper = new AccountMapper();
            if (resultSet.next()) {
                optionalAccount1 = Optional.of(accountMapper.extractFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return optionalAccount1.orElse(Account.newAccountBuilder().build());
    }

    @Override
    public Account findByName(String name) throws DaoException, SQLException {
        Optional<Account> optionalAccount = Optional.empty();
        try (PreparedStatement preparedStatement = connection.
                prepareCall(FIND_BY_NAME)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            AccountMapper accountMapper = new AccountMapper();
            if (resultSet.next()) {
                optionalAccount = Optional.ofNullable(accountMapper.extractFromResultSet(resultSet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return optionalAccount.orElse(Account.newAccountBuilder().build());
    }

    @Override
    public List<Account> findByRating(int rating) throws DaoException {
        List<Account> accountsRating = new ArrayList<>();

        try (PreparedStatement preparedStatementId = connection.
                prepareStatement(FIND_BY_RATING)) {
            preparedStatementId.setInt(1, rating);

            return getUserListExecute(accountsRating, preparedStatementId);
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public List<Account> findAllAccounts() throws DaoException {
        List<Account> accounts = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.
                prepareStatement(FIND_ALL)) {
            return getUserListExecute(accounts, preparedStatement);
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Appointment> findAllAppointments() throws DaoException {
        return null;
    }

    @Override
    public boolean updateAccount(int accountId, Account account) throws DaoException {
        try (PreparedStatement preparedStatement = connection.
                prepareStatement(UPDATE_USER)){
            setAccountParameters(account, preparedStatement);

            preparedStatement.setInt(8, accountId);

            int rowUpdated = preparedStatement.executeUpdate();

            if (rowUpdated == 7) {
                return true;
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return false;
    }

    @Override
    public boolean removeAccountById(int accountId) throws DaoException {
        try (PreparedStatement preparedStatement = connection.
                prepareStatement(REMOVE)){

            preparedStatement.setInt(1, accountId);
            int rowDeleted = preparedStatement.executeUpdate();

            if (rowDeleted > 0) {
                return true;
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return false;
    }

    private void setAccountParameters(Account account, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, account.getLogin());
        preparedStatement.setString(2, account.getPassword());
        preparedStatement.setString(3, account.getFirstName());
        preparedStatement.setString(4, account.getLastName());
        preparedStatement.setString(5, account.getPhoneNumber());
        //preparedStatement.setString(6, account.getUserRole().getRoleName());
        preparedStatement.setBigDecimal(7, account.getRating());
    }

    private List<Account> getUserListExecute(List<Account> accounts, PreparedStatement preparedStatement) throws SQLException {
        ResultSet resultSet1 = preparedStatement.executeQuery();

        AccountMapper accountMapper = new AccountMapper();

        while (resultSet1.next()) {
            Optional<Account> account = Optional.
                    ofNullable(accountMapper.extractFromResultSet(resultSet1));
            account.ifPresent(accounts::add);
        }
        return accounts;
    }

    @Override
    public void close() throws DaoException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}

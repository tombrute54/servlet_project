package com.example.servletProject.model.entity;

public class Status {

    private int id;
    private Appointment appointment;
    private String name;
    private String description;

    public int getId() {
        return id;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Status status = (Status) o;

        if (id != status.id) return false;
        if (!appointment.equals(status.appointment)) return false;
        if (!name.equals(status.name)) return false;
        return description.equals(status.description);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + appointment.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", appointment=" + appointment +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}

package com.example.servletProject.model.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class Service {
    private int id;
    private String serviceName;
    private String description;
    private BigDecimal price;
    private int durationMinutes;
    private List<Feedback> feedbackList = new ArrayList<>();
    private List<Appointment> appointmentSet = new ArrayList<>();


    public int getId() {
        return id;
    }


    public String getServiceName() {
        return serviceName;
    }


    public String getDescription() {
        return description;
    }


    public BigDecimal getPrice() {
        return price;
    }


    public int getDurationMinutes() {
        return durationMinutes;
    }


    public List<Feedback> getFeedbackList() {
        return feedbackList;
    }


    public List<Appointment> getAppointmentSet() {
        return appointmentSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Service)) {
            return false;
        }

        Service service = (Service) o;

        if (getId() != service.getId()) {
            return false;
        }
        if (getDurationMinutes() != service.getDurationMinutes()) {
            return false;
        }
        if (getServiceName() != null ? !getServiceName().equals(service.getServiceName()) : service.getServiceName() != null) {
            return false;
        }
        if (getDescription() != null ? !getDescription().equals(service.getDescription()) : service.getDescription() != null) {
            return false;
        }
        if (getPrice() != null ? !getPrice().equals(service.getPrice()) : service.getPrice() != null) {
            return false;
        }
        if (getFeedbackList() != null ? !getFeedbackList().equals(service.getFeedbackList()) : service.getFeedbackList() != null) {
            return false;
        }
        return getAppointmentSet() != null ? getAppointmentSet().equals(service.getAppointmentSet()) : service.getAppointmentSet() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getServiceName() != null ? getServiceName().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getPrice() != null ? getPrice().hashCode() : 0);
        result = 31 * result + getDurationMinutes();
        result = 31 * result + (getFeedbackList() != null ? getFeedbackList().hashCode() : 0);
        result = 31 * result + (getAppointmentSet() != null ? getAppointmentSet().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", serviceName='" + serviceName + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", durationMinutes=" + durationMinutes +
                ", feedbackList=" + feedbackList +
                ", appointmentSet=" + appointmentSet +
                '}';
    }


    public static ServiceBuilder newBuilder() {
        return new Service().new ServiceBuilder();
    }


    public class ServiceBuilder {

        private ServiceBuilder() {
        }


        public ServiceBuilder setId(int id) {
            Service.this.id = id;
            return this;
        }


        public ServiceBuilder setServiceName(String serviceName) {
            Service.this.serviceName = serviceName;
            return this;
        }


        public ServiceBuilder setDescription(String description) {
            Service.this.description = description;
            return this;
        }


        public ServiceBuilder setPrice(BigDecimal price) {
            Service.this.price = price;
            return this;
        }


        public ServiceBuilder setDurationMinutes(int durationMinutes) {
            Service.this.durationMinutes = durationMinutes;
            return this;
        }


        public ServiceBuilder setFeedbackList(List<Feedback> feedbackList) {
            Service.this.feedbackList = feedbackList;
            return this;
        }


        public ServiceBuilder setAppointments(List<Appointment> appointmentSet) {
            Service.this.appointmentSet = appointmentSet;
            return this;
        }


        public Service build() {
            return Service.this;
        }
    }
}
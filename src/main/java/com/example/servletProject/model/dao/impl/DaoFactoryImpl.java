package com.example.servletProject.model.dao.impl;



import com.example.servletProject.model.dao.*;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;


public class DaoFactoryImpl extends DaoFactory {
    private DataSource dataSource;

    {
        try {
            dataSource = ConnectionPoolHolder.getDataSource();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public AccountDAO createAccountDao() throws SQLException {
        return new AccountDaoImpl(dataSource.getConnection());
    }

    @Override
    public AppointmentDAO createAppointmentDao() throws SQLException {
        return new AppointmentDaoImpl(dataSource.getConnection());
    }

    @Override
    public RoleDAO createRoleDao() throws SQLException {
        return null;
    }


    @Override
    public FeedbackDAO createFeedbackDao() throws SQLException {
        return new FeedbackDaoImpl(dataSource.getConnection());
    }

    @Override
    public ServiceDAO createServiceDao() throws SQLException {
        return new ServiceDaoImpl(dataSource.getConnection());
    }

    @Override
    public StatusDAO createStatusDao() throws SQLException {
        return null;
    }
}

package com.example.servletProject.model.dao.mapper;

import com.example.servletProject.model.entity.Feedback;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class FeedbackMapper implements ObjectMapper<Feedback> {

    @Override
    public Feedback extractFromResultSet(ResultSet resultSet) throws SQLException {
        return null;
    }

    @Override
    public Feedback makeUnique(Map<Integer, Feedback> cache, Feedback object) {
        return null;
    }
}

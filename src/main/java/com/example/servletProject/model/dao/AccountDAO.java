package com.example.servletProject.model.dao;

import com.example.servletProject.model.entity.Account;
import com.example.servletProject.model.entity.Appointment;

import java.sql.SQLException;
import java.util.List;

public interface AccountDAO extends GenericDAO<Account>{

    Account registerAccount(Account account) throws DaoException;

    Account findById(int id) throws DaoException;

    Account findByLogin(String login) throws DaoException;

    Account findByName(String name) throws DaoException, SQLException;

    List<Account> findByRating(int rating) throws DaoException;

    List<Account> findAllAccounts() throws DaoException;

    List<Appointment> findAllAppointments() throws DaoException;

    boolean updateAccount(int AccountId, Account account) throws DaoException;

    boolean removeAccountById(int AccountId) throws DaoException;
}

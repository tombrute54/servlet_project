package com.example.servletProject.model.dao.mapper;

import com.example.servletProject.model.entity.Account;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class AccountMapper implements ObjectMapper<Account>{

    @Override
    public Account extractFromResultSet(ResultSet resultSet) throws SQLException {
        Map<Integer, Account> accounts = new HashMap<>();
        Account account = Account.newAccountBuilder().
                setId(resultSet.getInt("id")).
                setLogin(resultSet.getString("login_email")).
                setPassword(resultSet.getString("password_hash")).
                setFirstName(resultSet.getString("first_name")).
                setLastName(resultSet.getString("last_name")).
                setPhoneNumber(resultSet.getString("phone_number")).
                //setUserRole(Account.Role.valueOf(resultSet.getString("user_role"))).
                setRating(resultSet.getBigDecimal("rating")).
                build();
        accounts.put(account.getId(), account);
        account = this.makeUnique(accounts, account);
        return account;
    }

    @Override
    public Account makeUnique(Map<Integer, Account> cache, Account account) {
        cache.putIfAbsent(account.getId(), account);
        return cache.get(account.getId());
    }
}

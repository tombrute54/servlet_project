package com.example.servletProject.model.dao;

import com.example.servletProject.model.entity.Appointment;

import java.util.List;

public interface AppointmentDAO extends GenericDAO<Appointment>{

    Appointment createAppointment(Appointment appointment) throws DaoException;

    Appointment findAppointmentById(int id) throws DaoException;

   // List<Appointment> findAllByStatus(Appointment.Status status) throws DaoException;

    List<Appointment> findAllAvailable() throws DaoException;

   // List<User> findByUserIdAndDay(int userId, LocalDate dateTime) throws DaoException;

    Appointment findByUserId(int userId) throws DaoException;

    List<Appointment> findByServiceId(int orderId) throws DaoException;

    List<Appointment> findAll() throws DaoException;

    void updateAppointment(int appointmentId, Appointment appointment) throws DaoException;

    void cancelAppointment(int id) throws DaoException;

    void removeOldAppointments(List<Appointment> oldAppointments) throws DaoException;

    void removeAppointmentById(int id) throws DaoException;
}

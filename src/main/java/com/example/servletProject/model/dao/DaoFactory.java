package com.example.servletProject.model.dao;

//import com.training.app.model.dao.impl.DaoFactoryImpl;

import com.example.servletProject.model.dao.impl.DaoFactoryImpl;

import java.sql.SQLException;


public abstract class DaoFactory {

    private static DaoFactory daoFactory;


    public abstract AccountDAO createAccountDao() throws SQLException;


    public abstract AppointmentDAO createAppointmentDao() throws SQLException;


    public abstract RoleDAO createRoleDao() throws SQLException;


    public abstract FeedbackDAO createFeedbackDao() throws SQLException;


    public abstract ServiceDAO createServiceDao() throws  SQLException;

    public abstract StatusDAO createStatusDao() throws  SQLException;


    public static DaoFactory getInstance() {
        if (daoFactory == null) {
            synchronized (DaoFactory.class) {
                if (daoFactory == null) {
                    DaoFactory tmp = new DaoFactoryImpl();
                    daoFactory = tmp;
                }
            }
        }
        return daoFactory;
    }
}


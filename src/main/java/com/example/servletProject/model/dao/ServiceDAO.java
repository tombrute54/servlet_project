package com.example.servletProject.model.dao;

import com.example.servletProject.model.entity.Service;

import java.util.List;

public interface ServiceDAO extends GenericDAO<Service>{

    Service createService(Service service) throws DaoException;

    Service findServiceById(int id) throws DaoException;

    Service findServiceByName(String serviceName) throws DaoException;

    List<Service> findAll() throws DaoException;

    Service updateService(int serviceId, Service service) throws DaoException;

    Service removeServiceById(int serviceId) throws DaoException;
}

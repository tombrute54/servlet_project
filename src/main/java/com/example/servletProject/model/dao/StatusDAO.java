package com.example.servletProject.model.dao;

import com.example.servletProject.model.entity.Status;

public interface StatusDAO extends GenericDAO<Status> {

    Status createStatus(Status status) throws  DaoException;

    Status findByStatusId(int id) throws DaoException;

    Status findByStatusName(String name) throws DaoException;

    Status updateStatus (int id, String name) throws  DaoException;

    Status removeStatus (int id) throws DaoException;

}

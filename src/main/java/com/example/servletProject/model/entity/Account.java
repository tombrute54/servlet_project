package com.example.servletProject.model.entity;

import java.math.BigDecimal;
import java.util.*;


public class Account {

    public enum Role {

        ADMIN("admin"),

        EMPLOYEE("employee"),

        CLIENT("client"),

        GUEST("guest");

        private final String roleName;

        Role(String roleName) {
            this.roleName = roleName;
        }


        public String getRoleName() {
            return roleName;
        }
    }

    private int id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private int roleId;
    private BigDecimal rating;
    private List<Appointment> appointmentList = new ArrayList<>();


    public int getId() {
        return id;
    }

    public int getRoleId() {
        return roleId;
    }

    public String getLogin() {
        return login;
    }


    public String getPassword() {
        return password;
    }


    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }


//    public Role getAccountRole() {
//        return AccountRole;
//    }


    public BigDecimal getRating() {
        return rating;
    }


//    public Set<Card> getCards() {
//        return cards;
//    }


    public List<Appointment> getAppointmentList() {
        return appointmentList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }

        Account Account = (Account) o;

        if (getId() != Account.getId()) {
            return false;
        }
        if (getLogin() != null ? !getLogin().equals(Account.getLogin()) : Account.getLogin() != null) {
            return false;
        }
        if (getPassword() != null ? !getPassword().equals(Account.getPassword()) : Account.getPassword() != null) {
            return false;
        }
        if (getFirstName() != null ? !getFirstName().equals(Account.getFirstName()) : Account.getFirstName() != null) {
            return false;
        }
        if (getLastName() != null ? !getLastName().equals(Account.getLastName()) : Account.getLastName() != null) {
            return false;
        }
        if (getPhoneNumber() != null ? !getPhoneNumber().equals(Account.getPhoneNumber()) : Account.getPhoneNumber() != null) {
            return false;
        }
//        if (getAccountRole() != Account.getAccountRole()) {
//            return false;
//        }
        if (getRating() != null ? !getRating().equals(Account.getRating()) : Account.getRating() != null) {
            return false;
        }
//        if (getCards() != null ? !getCards().equals(Account.getCards()) : Account.getCards() != null) {
//            return false;
//        }
        return getAppointmentList() != null ? getAppointmentList().equals(Account.getAppointmentList()) : Account.getAppointmentList() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getLogin() != null ? getLogin().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPhoneNumber() != null ? getPhoneNumber().hashCode() : 0);
       // result = 31 * result + (getAccountRole() != null ? getAccountRole().hashCode() : 0);
        result = 31 * result + (getRating() != null ? getRating().hashCode() : 0);
      //  result = 31 * result + (getCards() != null ? getCards().hashCode() : 0);
        result = 31 * result + (getAppointmentList() != null ? getAppointmentList().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
               //", AccountRole=" + AccountRole +
                ", rating=" + rating.doubleValue() +
             //   ", cards=" + cards +
                ", appointmentList=" + appointmentList +
                '}';
    }


    public static AccountBuilder newAccountBuilder() {
        return new Account().new AccountBuilder();
    }


    public class AccountBuilder {
        private AccountBuilder() {
        }


        public AccountBuilder setId(int id) {
            Account.this.id = id;
            return this;
        }


        public AccountBuilder setLogin(String login) {
            Account.this.login = login;
            return this;
        }


        public AccountBuilder setPassword(String password) {
            Account.this.password = password;
            return this;
        }


        public AccountBuilder setFirstName(String firstName) {
            Account.this.firstName = firstName;
            return this;
        }


        public AccountBuilder setLastName(String lastName) {
            Account.this.lastName = lastName;
            return this;
        }


        public AccountBuilder setPhoneNumber(String phoneNumber) {
            Account.this.phoneNumber = phoneNumber;
            return this;
        }


//        public AccountBuilder setAccountRole(Account.Role AccountRole) {
//            Account.this.AccountRole = AccountRole;
//            return this;
//        }


        public AccountBuilder setRating(BigDecimal rating) {
            Account.this.rating = rating;
            return this;
        }


//        public AccountBuilder setCards(Set<Card> cards) {
//            Account.this.cards = cards;
//            return this;
//        }


        public AccountBuilder setAppointments(List<Appointment> appointmentList) {
            Account.this.appointmentList = appointmentList;
            return this;
        }


        public Account build() {
            return Account.this;
        }
    }

}
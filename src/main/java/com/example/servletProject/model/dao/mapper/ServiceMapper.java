package com.example.servletProject.model.dao.mapper;

import com.example.servletProject.model.entity.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


public class ServiceMapper implements ObjectMapper<Service>{

    @Override
    public Service extractFromResultSet(ResultSet resultSet) throws SQLException {
        Map<Integer, Service> serviceMap = new HashMap<>();
        Service service = Service.newBuilder().
                setId(resultSet.getInt("id")).
                setServiceName(resultSet.getString("name")).
                setDescription(resultSet.getString("description")).
                setPrice(resultSet.getBigDecimal("price")).
                setDurationMinutes(resultSet.getInt("duration_minutes")).
                build();
        serviceMap.put(service.getId(), service);
        service = this.makeUnique(serviceMap, service);

        return service;

    }

    @Override
    public Service makeUnique(Map<Integer, Service> cache, Service service) {
        cache.putIfAbsent(service.getId(), service);
        return cache.get(service.getId());
    }
}
